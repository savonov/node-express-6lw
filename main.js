'use strict';

const {
  app,
  BrowserWindow
} = require('electron')
const locals = { /* ...*/ }
const setupPug = require('electron-pug')
const server = require('./src/app');

// Standard stuff

app.on('ready', async () => {
  try {
    let pug = await setupPug({
      pretty: true
    }, locals)
    pug.on('error', err => console.error('electron-pug error', err))
  } catch (err) {
    // Could not initiate 'electron-pug'
  }

  let mainWindow = new BrowserWindow({
    width: 1200,
    height: 600
  })

  mainWindow.loadURL(`http://localhost:1139`)

  // the rest...
})