const express = require('express');
const http = require('http');
const pug = require('pug');
const fetch = require('node-fetch');
const util = require('util');

const app = express();

app.set('view engine', 'pug');

app.use('/public', express.static(__dirname + '/public'));

var indexRouter = require('./routes/index');
var carRouter = require('./routes/car');
var clientRouter = require('./routes/client');
var contractRouter = require('./routes/contract');
var contactRouter = require('./routes/contact');
var aboutRouter = require('./routes/about');

app.use('/', indexRouter);
app.use('/car', carRouter);
app.use('/client', clientRouter);
app.use('/contract', contractRouter);
app.use('/contact', contactRouter);
app.use('/about', aboutRouter);

app.listen(1139)