const express = require('express');
const router = express.Router();
const http = require('http');

router.get('/', (req, res) => {

  http.get('http://localhost:1111/api/car', (res) => {

    res.setEncoding('utf8');

    let rawData = '';

    res.on('data', (chunk) => {
      rawData += chunk;
    });

    res.on('end', () => {
      try {
        const cars = JSON.parse(rawData);
        renderCars(cars);
      } catch (e) {
        console.error(e.message);
      }
    });
  })

  function renderCars(cars) {
    res.render(`../src/views/car/index`, {
      title: 'Work with Car ',
      cars: cars
    });
  }
});


module.exports = router;