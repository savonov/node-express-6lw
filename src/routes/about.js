const express = require('express');
const router = express.Router();
const http = require('http');

router.get('/', (req, res) => {
  res.render(`../src/views/about`, {
    title: 'About us',
  });
});

module.exports = router;