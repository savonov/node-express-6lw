const express = require('express');
const router = express.Router();
const http = require('http');


router.get('/', (req, res) => {
  res.render(`../src/views/index`, {
    title: 'Work with API (CRUD)',
  });
});

module.exports = router;