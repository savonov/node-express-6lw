const express = require('express');
const router = express.Router();
const http = require('http');

router.get('/', (req, res) => {

  http.get('http://localhost:1111/api/client', (res) => {

    res.setEncoding('utf8');

    let rawData = '';

    res.on('data', (chunk) => {
      rawData += chunk;
    });

    res.on('end', () => {
      try {
        var clients = JSON.parse(rawData);
        renderClients(clients);
      } catch (e) {
        console.error(e.message);
      }
    });
  })

  function renderClients(clients) {
    res.render(`../src/views/client/index`, {
      title: 'Work with Client ',
      clients: clients
    });
  }
});

module.exports = router;