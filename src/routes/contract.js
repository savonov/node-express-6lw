const express = require('express');
const router = express.Router();
const http = require('http');


router.get('/', (req, res) => {

  http.get('http://localhost:1111/api/contract', (res) => {

    res.setEncoding('utf8');

    let rawData = '';

    res.on('data', (chunk) => {
      rawData += chunk;
    });

    res.on('end', () => {
      try {
        var contracts = JSON.parse(rawData);
        renderContracts(contracts);
      } catch (e) {
        console.error(e.message);
      }
    });
  })

  function renderContracts(contracts) {
    res.render(`../src/views/contract/index`, {
      title: 'Work with Contracts ',
      contracts: contracts
    });
  }
});

module.exports = router;